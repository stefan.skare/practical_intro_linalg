%% Chapter 3.6.2 – Mass on a vertical spring with damping
%  Based on: https://www.kalmanfilter.net/modeling.html

% Simulation parameters
m = 4;       % mass [kg]
d = 1;       % damper coefficient
s = 8;       % spring coefficient
g = -9.82;   % gravitational acceleration [m/s^2]
T = 10;      % max time [s]
h = 0.001;   % small update steps in [s]. Recommended <= 0.001
x0 = [0 0]'; % Initial value: position = 0, speed = 0

% Dynamics matrix A
A = [0       1
    -s/m   -d/m];

% Input matrix B
B = [0
     g];

% Output matrix C (no effect here)
C  = [0 0];

% Feedthrough matrix D (no effect here)
D = 0;

%--------Update state vector using linear algebra in small steps ---------------
I = eye(2); % identity matrix
t = 0:h:T; % array with time steps
x = zeros(2, numel(t)); % size [2,np.size(t)] to plot the history over T
x(:,1) = x0; % assign x0 to the first column

% Update the state vector x[:,k+1] from x[:,k] for each time step k
for k = 1:(numel(t)-1)
  x(:,k+1) = (A*h + I) * x(:,k) + B*h; % update equation
                                       % (needs small 'h' to work well)
end

% Plot the result with dots (red for position, green for velocity)
% plot every skip'th column in x to avoid the dots to overlap
skip = round(1/(10*h));

clf % clear figure
% position (x1)
subplot(2,1,1); hold on
plot(t(1:skip:end),x(1,1:skip:end),'r.', 'MarkerSize', 30); % red dots

% velocity (x2)
subplot(2,1,2); hold on
plot(t(1:skip:end),x(2,1:skip:end),'g.', 'MarkerSize', 30); % green dots

% OPTIONAL: Compare with the result with a continous-time solution using 'ss' and
% 'lsim' (as accurate reference). Note that Matlab function 'ss' requires one of 
% the following Matlab toolboxes installed:
% Control System, DSP System, Model Predictive Control, or Signal Processing

% time steps for state-space model, in steps of 0.1 s instead of h
tcont = 0:0.1:T;
u = ones(size(tcont));

% create state space model
sys = ss(A,B,C,D);

% simulate time response of dynamic system
[~,~,xref] = lsim(sys,u,tcont,x0);

hold on; % add to previous plot

subplot(2,1,1); % position (x1)
plot(tcont, xref(:,1), 'r-', 'Linewidth', 2);

subplot(2,1,2); % velocity (x2)
plot(tcont, xref(:,2), 'g-', 'Linewidth', 2);