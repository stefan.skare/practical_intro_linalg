%% Chapter 5.5 - Life expectancy vs. year and countries

lifeexpect_history = readtable('c55_lifeexpect_history.csv');

L = table2array(lifeexpect_history(:,2:end)); % L = lifeexpectancy (2D array)

c = size(L,1); % # countries
t = size(L,2); % # years

p = 4;
q = 2;

% polynomial model of degree 'q' for country
Acountry = makepolymodelA(c, q+1,'normalize');

% polynomial model of degree 'p' for year
Atime    = makepolymodelA(t, p+1,'normalize');

A = kron(Atime, Acountry); % Kronecker product of the two matrices

y = L(:); % Make a 1D column vector from the 2D life expectancy data

x = A \ y;

% Compute fitted life expectancy values
yhat = A * x;
Lhat = reshape(yhat, size(L)); % reshape back to a 2D array like 'L'

% plot the original data + fitted surface
figure(1); clf; 
[Xcoords,Ycoords] = ndgrid(1:size(L,1), (0:(size(L,2)-1))+1800);
surf(Xcoords, Ycoords, L, 'FaceAlpha', 0.2, 'EdgeAlpha', 0); % original data
hold on
surf(Xcoords, Ycoords, Lhat, 'LineWidth', 2); % fitted surface

set(gca,'XTick',[1 50 100 150 183], 'YTick', ...
        1800+[0 50 100 150 200 220],'ZTick',[0 10:10:90])
camproj perspective
xlabel('Country index')
ylabel('Year')
zlabel('Life expectancy')
view(57,22)
colormap(jet)
axis([1 183 1800 2020 0 90])
