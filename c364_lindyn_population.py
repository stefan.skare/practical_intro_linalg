## Chapter 3.6.4 – Population modeling
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# load population data vectors as a table from comma separated file
data2019 = pd.read_csv('swepopulationdata2019.csv')

# The loaded 100-element columns in table 'data2019' are:
# Column ages: 0:99
# Column x0:   population 2019
# Column b:    birth rate 2019
# Column d:    death rate 2019
# Column u:    net immigration 2019

# Part 1: Show the loaded data (year 2019)
fig,axs = plt.subplots(2,2)

axs[0,0].plot(data2019.ages, data2019.x0,'m')
axs[0,0].set_title('Population 2019'); axs[0,0].set_xlabel('Age')

axs[0,1].plot(data2019.ages, data2019.b, 'b') 
axs[0,1].set_title('Birth rate 2019'); axs[0,0].set_xlabel('Age')

axs[1,0].plot(data2019.ages, data2019.d, 'g')
axs[1,0].set_title('Death rate 2019'); axs[0,0].set_xlabel('Age')

axs[1,1].plot(data2019.ages, data2019.u, 'r')
axs[1,1].set_title('Net immigration 2019'); axs[0,0].set_xlabel('Age')

plt.show(block=False)

print('Total population 2019: {0} M'.format(round(sum(data2019.x0)/1e6, 2)))
print('# births 2019: {0} thousands'.format(round(sum(data2019.b * data2019.x0)/1e3, 2)))
print('# deaths 2019: {0} thousands'.format(round(sum(data2019.d * data2019.x0)/1e3, 2)))
print('Net immigration 2019:  {0} thousands'.format(round(sum(data2019.u)/1e3)))


## Part 2: Population dynamics 2019-2100 (82 years total)
b = data2019.b # use same birth rate as in 2019
d = data2019.d # use same death rate as in 2019

# first try half (0.5*) the net immigration as in 2019
u = np.asmatrix(0.5 * data2019.u).T  # try also other factors!

simyears = 82 # 82 years (2019-2100)

# Build up the Leslie matrix ("A") from the birth and death rates of 2019
A = np.asmatrix(np.roll(np.diag(1-data2019.d), 1, axis=0)) # first create the values 
                                                           # from the death rates
A[0]= np.asmatrix(b)                    # then add the birth rates as the first row of A

x = np.asmatrix(np.zeros((np.size(data2019.x0), simyears)))
x[:,0] = np.asmatrix(data2019.x0).T # first state of x is the 2019 population distribution

# Do the simulation
for k in range(1, simyears):
  x[:,k] = A @ x[:,k-1] + u

print('Total population 2019: {0} M'.format(round(sum(data2019.x0)/1e6, 2)))
print('Total population 2029: {0} M'.format(round(sum(x[:,10]).item()/1e6, 2)))
print('Total population 2050: {0} M'.format(round(sum(x[:,31]).item()/1e6, 2)))
print('Total population 2100: {0} M'.format(round(sum(x[:,81]).item()/1e6, 2)))

fig2,axs2 = plt.subplots(1)
axs2.plot(data2019.ages, x[:,0], '-*', label='2019')
axs2.plot(data2019.ages, x[:,10], label='2029') # 10 = year 2029
axs2.plot(data2019.ages, x[:,31], label='2050') # 31 = year 2050
axs2.plot(data2019.ages, x[:,81], label='2100') # 81 = year 2100
axs2.legend()
axs2.set_xlabel('Age')
axs2.set_ylabel('# of people having this age')
plt.show()
