%% Chapter 5.6 - Regressors for Life expectancy

Ltable = readtable('c56_regressors_gapminderorg.csv');
Ltable = Ltable(:,2:end); % remove 1st column with country names
L = table2array(Ltable); % convert from table datatype to 2D array


%% Figure 1: Plot life expectancy and regressors as gray scale bars
figure(1); clf; colormap(gray(256))

for j = 1:width(Ltable)
  subplot(1, width(Ltable), j)
  imagesc(L(:,j))
  if j == 1
    ylabel('Country index (sorted by life expectancy)')
  end
  xlabel(Ltable.Properties.VariableNames{j},'interpreter','none')
end

%% Figure 2: Scatter plot regressors vs life expectancy with fitted line
figure(2); clf

y = L(:,1); % life expectancy is the 1st column
regressors = L(:,2:end);
% get the names of the column headers of the table
regressor_names = Ltable.Properties.VariableNames(2:end);
const = ones(size(L(:,1)));

do_log_some_regressors = 1; % 1 = yes, 0 = no. Why not try both?
if do_log_some_regressors
  regressors(:,3:end) = log(regressors(:,3:end));
  regressor_names(3:end) = cellfun(@(x) ['log(' x ')'], ...
                           regressor_names(3:end), 'UniformOutput', false);
end

% Reference error norm 'Rconst'
% We can use this when calculating the relative error norm below
Rconst = norm(y - mean(y)); 

for r = 1:size(regressors,2)
  A = [const regressors(:,r)]; % model a constant + one regressor at a time
  xhat = A \ y;
  yhat = A * xhat;
  tmp = corrcoef(regressors(:,r), y); % returns a 2x2 matrix, but ...
  rcorr(r) = tmp(1,2); % ... we need only one of the non-1 values in that matrix
  R(r) = norm(y - yhat) / Rconst; % Relative error norm

  subplot(2, 3, r);
  plot(regressors(:,r), y, '.'); hold on
  plot(regressors(:,r), yhat, '-');
  xlabel(regressor_names{r},'Interpreter', 'None');
  ylabel('Life expectancy')
  legend(sprintf('R=%0.3f  r=%.3f', R(r), rcorr(r)))
  axis padded
end


%% Figure 3: The relative error norm for one regressor at a time and combined
figure(3); clf

for r = 1:size(R,2)
  plot([0 1], [1 R(r)],'o-'); hold on
  text(1, R(r), regressor_names{r},'interpreter', 'none', 'verticalalignment', 'top')
end
axis([0 size(R,2) 0 1])
grid on
xlabel('Number of regressors included in model matrix A')
ylabel('Relative error norm R')
axis([0 1.5 0 1])

% Let's sort the regressors by their relative error norm from the fit to life
% expectancy. This moves the most correlating regressor to the first column and 
% the least to the last column. We will use this below to see what happens when
% more than one regressor is used in the model matrix A
[~, s] = sort(abs(R),'ascend');
regressors_sorted = regressors(:,s);
regressor_names_sorted = regressor_names(s);

% Now, try adding more regressors and see if the relative norm 
% decreases much further

for r = 1:size(R,2)

  % increase the number of columns of A as 'r' increases
  A = [const regressors_sorted(:,1:r)]; 
  xhat = A \ y;
  yhat = A * xhat;
  Rcomb(r) = norm(y - yhat) / Rconst;

  if r >= 2
    plot([r-1, r], [Rcomb(r-1), Rcomb(r)],'ok-'); hold on
    text(r, Rcomb(r)-0.005, char(regressor_names_sorted{1:r}),'interpreter','none', 'verticalalignment', 'top')
  end
end
axis([0 size(R,2)+1 0 1])


%% Figure 4: Make a correlation matrix between all vectors
figure(4); clf

corrdata = [regressors y];
C = corrcoef(corrdata); 
clf; imshow(imresize(C,50,'nearest'),[-1 1]); colormap jet; colorbar
