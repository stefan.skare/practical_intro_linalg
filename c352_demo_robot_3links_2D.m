% Initial angles of the joints of the robot
q0_start = 0;   q0 = q0_start;
q1_start = 175; q1 = q1_start;
q2_start = -45; q2 = q2_start;

% How many degrees each joint should rotate from its starting position
q0_move = 23;
q1_move = -79;
q2_move = -48;

stepsize = 0.02; % Step size for the plotting animation (lower value => slower animation)

% First animate the motor at p0 by rotating it by q0_move degrees 
for t = 0:stepsize:1
  q0 = q0_start + q0_move * t; % change q0 as t goes from 0 to 1
  p = get_3links_coords(q0, q1, q2);
  plot_3links(p);
end

% Then animate the motor at p1 by rotating it by q1_move degrees
for t = 0:stepsize:1
  q1 = q1_start + q1_move * t; % change q1 as t goes from 0 to 1
  p = get_3links_coords(q0, q1, q2);
  plot_3links(p);
end

% Finally, animate the motor at p2 by rotating it by q2_move degrees
for t = 0:stepsize:1
  q2 = q2_start + q2_move * t; % change q2 as t goes from 0 to 1
  p = get_3links_coords(q0, q1, q2);
  plot_3links(p);
end

% Alternatively, we can move all motors together
q0 = q0_start; q1 = q1_start; q2 = q2_start; % reset to start position

for t = 0:stepsize:1
  q0 = q0_start + q0_move * t; % change q0 as t goes from 0 to 1
  q1 = q1_start + q1_move * t; % change q1 as t goes from 0 to 1
  q2 = q2_start + q2_move * t; % change q2 as t goes from 0 to 1
  p = get_3links_coords(q0, q1, q2);
  plot_3links(p);
end