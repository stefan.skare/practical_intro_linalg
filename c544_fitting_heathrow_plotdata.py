import matplotlib.pyplot as plt
import numpy as np

def c544_fitting_heathrow_plotdata(t, y, yhat, xest):
	
	metrics = ['Temperature', 'WindSpeed', 'AirPressure']
	k = 0
	fig,axs = plt.subplots(5, 3)

	for zoom in range(1,6):
		for m in range(len(metrics)):
			k += 1
			ax = axs[zoom-1, m]
			
			if zoom < 5:
				ax.plot(1949+t, y[:, m], 'k')
				ax.plot(1949+t, yhat[:, m], 'b')
			else:
				ax.bar(np.arange(len(xest)), xest[:, m], color='g')
				
			if zoom == 1:
				ax.set_title(metrics[m])
			
			if m == 0:
				if zoom == 1:
					ax.set_ylabel('72 years')
				elif zoom == 2:
					ax.set_ylabel('1980-1989')
				elif zoom == 3:
					ax.set_ylabel('Year 1984')
				elif zoom == 4:
					ax.set_ylabel('July 1984')

			ax.set_xticks([])
			ax.autoscale(enable=True,axis='y',tight=True)
			ax.margins(x=0,y=0.05)
   	
			if zoom == 1:
				ax.set_xlim(auto=True)
			elif zoom == 2:
				ax.set_xlim([1980,1989])
			elif zoom == 3:
				ax.set_xlim([1984,1985])
			elif zoom == 4:
				ax.set_xlim([1984.5,1984.5+30/365])
			elif zoom == 5:
				ax.set_xlabel('x')
 		
	plt.show()
