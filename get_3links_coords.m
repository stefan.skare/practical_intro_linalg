% Input:  Angles in degrees of the three rotating joints
% Output: 2x4 vector containing four 2D coordinates
function p = get_3links_coords(q0, q1, q2)

% Robotic links: [origin]---L0----[p1]---L1---[p2]-L2-[p3]

% Lengths of each link of the robotic arm
L0 = 1.6; % length of first link (one end fixed to the origin)
L1 = 1.4; % length of the second link (attached to the first and third links)
L2 = 0.3; % length of the third link (attached to the second link)
% The last link is also called the 'end-effector'

% Transformation matrices needed (2 translation matrices, 3 rotation matrices)
T0 = [1 0 L0
  0 1 0
  0 0 1]; % shifts the reference frame by the length of the first link

T1 = [1 0 L1
  0 1 0
  0 0 1]; % shifts the reference frame by the length of the second link

% 'cosd','sind' since inputs are in degrees

% rotation around the first joint
R0  = [cosd(q0) -sind(q0) 0
  sind(q0)  cosd(q0) 0
  0         0        1];

% rotation around the second joint
% (in the reference frame of the second joint)
R1  = [cosd(q1) -sind(q1) 0
  sind(q1)  cosd(q1) 0
  0         0        1];
% rotation around the third joint
% (in the reference frame of the third joint)
R2  = [cosd(q2) -sind(q2) 0
  sind(q2)  cosd(q2) 0
  0         0        1];

% First point ('p0') is the origin
p0 = [0; 0; 1]; % last 1 is the 1-extension
% first two values = the origin coordinate

% Second point ('p1') is where the first and second link meet
% Coordinate [L0,0]: The first link, horizontal with (0,0)
% being joint p0 (origin)
p1 = [L0; 0; 1];
p1 = R0 * p1;  % Rotate p1 around the origin using R0

% Third point ('p2') is where the second and third link meet
p2 = [L1; 0; 1]; % Coordinate [L1,0]: The second link, horizontal in its
% own reference frame with (0,0) being joint p1

% Then multiply from right to left:
% by R1 to rotate point p2 in its own reference frame by q1 degrees
% by T0 to account for the length of the first link
% by R0 to account for the rotation of the first joint at the origin
% p2 is now in the same global reference frame as p0 and p1
p2 = R0 * T0 * R1 * p2;

% Fourth point ('p3') is the tip of the third link,
% and the tip of the entire robotic arm
p3 = [L2; 0; 1]; % Coordinate [L2,0]: The third link, horizontal in its
% own reference frame with (0,0) being joint p2

% Then multiply from right to left:
% by R2 to rotate point p3 in its own reference frame by q2 degrees
% by T1 to account for the length of the second link
% by R1 to account for the rotation of the second joint at p1
% by T0 to account for the length of the first link
% by R0 to account for the rotation of the first joint at the origin
% p3 is now in the same global reference frame as p0, p1, and p2
p3 = R0 * T0 * R1 * T1 * R2 * p3;

% return the all points in a 2x4 array
p = [p0 p1 p2 p3];

end
