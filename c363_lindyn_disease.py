import numpy as np
import matplotlib.pyplot as plt

#---- Probabilities per week (vaguely similar to the Covid19 pandemic)
Psi = 0.0008
Pir = 0.35
Pis = 0.05
Pid = 0.004
Prs = 0.03

# Dynamics matrix
A = np.array([[(1-Psi), Pis,             Prs,     0],
              [Psi,     (1-Pis-Pir-Pid), 0,       0], 
              [0,       Pir,             (1-Prs), 0], 
              [0,       Pid,             0,       1]])

x0 = np.array([[8e9],
               [0],
               [0],
               [0]]) # 8 billion susceptible people in the beginning

numweeks = 52 * 2 # simulate 2 years (104 weeks)

# array with zeros to store the 104 weeks of (4x1) state vectors
x = np.asmatrix(np.zeros((4, numweeks))) 

x[:,0] = x0

for t in range(0, numweeks-1):
  x[:,t+1] = A @ x[:,t]

#--------------------------- Plotting the result -----------------------------

fig,axs = plt.subplots(1,2)

# Left plot (showing susceptible and recovered)
axs[0].plot(x[0,:].T/1e9, 'r', label='$x_s$ (susceptible)')
axs[0].plot(x[2,:].T/1e9, 'sienna', label='$x_r$ (recovered)')
axs[0].set_xlabel('Weeks after first infected case')
axs[0].set_ylabel('Billion people ($10^9$)')
axs[0].set_xlim([0, numweeks])
axs[0].set_ylim([0, 10])
axs[0].legend()

# Right plot (showing infected and dead)
axs[1].plot(x[1,:].T/1e6, 'g-', label='$x_i$ (infected)')
axs[1].plot(x[3,:].T/1e6, 'b-', label='$x_d$ (dead)')
axs[1].set_xlabel('Weeks after first infected case')
axs[1].set_ylabel('Million people ($10^6$)')
axs[1].set_xlim([0, numweeks])
axs[1].set_ylim([0, 20])
axs[1].legend()

plt.show()
