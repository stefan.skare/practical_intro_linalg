%% Chapter 5.2.2 – Temperature modeling 1920-1990. Extrapolation 1990-2019

% load temperature changes over the 20th century
tdata = readtable('c521_fitting_temp_data.csv');
years = 0:99; % One hundred years (1920-2019)

fityears = years(1:71); % 1920-1990
extrapolateyears = years(72:end); % 1991-2019

% use only the first 71 elements for fitting, simulating what was known 1990
y100 = tdata.dt;
y_fitthis = y100(1:71);
y_extrapolatethis = y100(72:end);

clf

option = 'normalize';

for p = 1:6 % polynomial degrees 0->5

  subplot(2,3,p);
  plot(fityears, y_fitthis, 'ko-','linewidth', 2); hold on;
  plot(extrapolateyears, y_extrapolatethis, 'ko:','linewidth', 1); hold on;

  % makepolymodelA(): Adding a third argument 'normalize' scales the column
  % vectors of A to the same L2 norm
  % The matrix A becomes then more stable to invert (lower condition number)
  % To get the same values for xhat, we post-divide with these norm scalings
  [A, normscale] = makepolymodelA(fityears, p, option);
  xhat = A \ y_fitthis;
  yhat_71years = A * xhat;
  if strcmp(option, 'normalize')
    xhat = xhat ./ normscale; % now adjust xhat due to 'normalize'
  end
  rnorm = norm(y_fitthis-yhat_71years);

  A_100years = makepolymodelA(years, p);
  yhat_100years = A_100years * xhat;
  rnorm100 = norm(y100-yhat_100years);
  plot(fityears, yhat_71years, 'b-','linewidth', 5);
  plot(years, yhat_100years, 'r--','linewidth',3);

  xlabel('years after 1920')
  ylabel('Temperature change [C]')
  title(sprintf('Polynomial fit - degree %d', p-1));
  text(10, 0.8, {'Poly coeff. (x_{hat}) =' ; sprintf('%.5f\n', xhat)})
  text(50, 1, sprintf('||r|| = %.2f\n', rnorm100))
  axis([-1 101 -0.2 1.5])
  axis square
  legend('y used in model','unseen y','y_{hat,71 years}', ...
         'y_{hat,extrapol. to 100 years}', 'location', 'NorthWest')
         
end

