import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from c544_fitting_heathrow_plotdata import c544_fitting_heathrow_plotdata

# Load Heathrow weather since 1949
LHR = pd.read_csv('c544_heathrowweather.csv')

## Convert date & time to decimal days and hours since 1949-01-01 @ 00:00:00
# - We need to do this as the months have different number of days and some 
# years have leap days
days_in_solaryear = 365.256363  # https://en.wikipedia.org/wiki/Tropical_year
# number of hours per solar year:
hours_in_solaryear = int(round(days_in_solaryear * 24))
# For each observation, get number of (decimal) days & hours since 
# the first measurement 
decimaldays = pd.to_datetime(LHR.Date) + pd.to_timedelta(LHR.Hour, unit='h')
decimaldays = decimaldays - pd.to_datetime(LHR.Date[0])
decimaldays = np.array(decimaldays.dt.total_seconds()/(24*3600)).T

accumulatedhours = np.int32(np.round(decimaldays * 24))
n = accumulatedhours[-1] + 1
ndays = int(np.round(decimaldays[-1]))
nyears = int(np.round(ndays/days_in_solaryear))
decimalyears_incl_missingdata = np.linspace(0, nyears, n)

# fill in data for hours having observations since 1949.
# size(y) = (hours between 1st and last) x (3 metrics)
y = np.empty([n, 3])
y[:] = np.nan # Initialize with NaN to create 'keeprows' vector below
              # (indicating hours with observations)
y[accumulatedhours, 0] = LHR.Temperature
y[accumulatedhours, 1] = LHR.WindSpeed
y[accumulatedhours, 2] = LHR.AirPressure

# true/false vector, where true means hours that have observation
# This is used to remove rows of the model matrix that have no measured data,
# make also 'keeprows' a simple 1D np.array() to allow indexing
keeprows = ~np.isnan(y.sum(axis=1))

# np.size(ynorm) = 1x3. Norms of each weather metric vector:
ynorm = np.linalg.norm(y[keeprows,:], axis=0)


## Model matrices A**, modeling effects at different time scales 

# Linear modeling over 72 years
# since the 2nd column has
# - zero mean, it will not contribute to the mean
# - range 1, the value of xlin[1] will represent how much each metric 
# changes over the entire period
Alin = np.column_stack((np.ones(n), np.linspace(-0.5, 0.5, n)))

# Seasonal modeling (repeated over all 72 years)
Aoneyear = np.kron(np.eye(366), np.ones((24,1)))  # first 366 days
# remove the last 18 hours of the 366th day
Aoneyear = Aoneyear[:hours_in_solaryear, :]
# stack Aoneyear vertically 'nyears' times
Ayear = np.tile(Aoneyear, (nyears, 1)) 

# Hourly modeling (repeated over all 72 years)
Aday = np.tile(np.eye(24), (ndays, 1))

## [Alin] - linear trend (over 72 years)
# xlin: mean + linear trend
xlin = np.linalg.lstsq(Alin[keeprows,:], y[keeprows,:], rcond=None)[0]
yhat = Alin @ xlin
# what cannot be explained by the linear model:
r = y[keeprows,:] - yhat[keeprows,:] 
# ratio of norm of residual 'r' and the norm of the data 'y':
normratio_lin = np.linalg.norm(r, axis=0) / ynorm
print(f"Linear model norm ratio: Temp = {normratio_lin[0]:.4g}, Wind = {normratio_lin[1]:.4g}, Pressure = {normratio_lin[2]:.4g}")
c544_fitting_heathrow_plotdata(decimalyears_incl_missingdata, y, yhat, xlin)

## [Ayears] - model seasonal effects after removing linear trend
# detrend the weather data by subtracting linear fit 'yhat'
y1 = y - yhat  
xyear = np.linalg.lstsq(Ayear[keeprows,:], y1[keeprows,:], rcond=None)[0]
y1hat = Ayear.dot(xyear)
# what cannot be explained by linear + seasonal models:
r = y1[keeprows,:] - y1hat[keeprows,:]
# ratio of norm of residual 'r' and the norm of the data 'y':
normratio_linyear = np.linalg.norm(r, axis=0) / ynorm
print(f"Linear + seasonal model norm ratio: Temp = {normratio_linyear[0]:.4g}, Wind = {normratio_linyear[1]:.4g}, Pressure = {normratio_linyear[2]:.4g}")
c544_fitting_heathrow_plotdata(decimalyears_incl_missingdata, y, yhat + y1hat, xyear)

## [Adays] - model hourly effects after removing seasonal effects and linear trend
y2 = y1 - y1hat  # remove seasonal trends
xday = np.linalg.lstsq(Aday[keeprows,:], y2[keeprows,:], rcond=None)[0]
y2hat = Aday.dot(xday)
# what cannot be explained by linear + seasonal + hourly models:
r = y2[keeprows,:] - y2hat[keeprows,:]
# ratio of norm of residual 'r' and the norm of the data 'y':
normratio_all = np.linalg.norm(r, axis=0) / ynorm  
print(f"Linear + seasonal + day model norm ratio: Temp = {normratio_all[0]:.4g}, Wind = {normratio_all[1]:.4g}, Pressure = {normratio_all[2]:.4g}")
c544_fitting_heathrow_plotdata(decimalyears_incl_missingdata, y, yhat + y1hat + y2hat, xday)