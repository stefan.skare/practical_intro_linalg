function c544_fitting_heathrow_plotdata(t, y, yhat, xest)

metrics = {'Temperature', 'WindSpeed','AirPressure'};

k = 0;

for zoom = 1:5
  for m = 1:numel(metrics)
    k = k+1;
    subplot(5, numel(metrics), k)
    if zoom < 5
      hold off; plot(1949+t, y(:,m), 'k');
      hold on;  plot(1949+t, yhat(:,m), 'b');
    else
      bar(xest(:,m),'FaceColor', 'g');
    end
    if zoom == 1
      title(metrics{m})
    end
    if m == 1
      if zoom == 1
        ylabel('72 years')
      elseif zoom == 2
        ylabel('1980-1989')
      elseif zoom == 3
        ylabel('Year 1984')
      elseif zoom == 4
        ylabel('July 1984')
      end
    end

    a = gca;
    set(a,'XTick',[])
    axis padded;
    
    if zoom == 1
      axis tight
    elseif zoom == 2
      a.XLim = [1980 1989];
    elseif zoom == 3
      a.XLim = [1984 1985];
    elseif zoom == 4
      a.XLim = [1984.5 1984.5+30/365];
    elseif zoom == 5
      xlabel('x')
    end
  end
end
