## Chapter 5.5 - Life expectancy vs. year and countries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from makepolymodelA import makepolymodelA

# Load the data
lifeexpect_history = pd.read_csv('c55_lifeexpect_history.csv')

# Extract life expectancy data and country/year indices
L = np.asmatrix(lifeexpect_history.iloc[:, 1:].values)
c = L.shape[0]
t = L.shape[1]

# Set polynomial degrees
p = 4
q = 2

# Create polynomial models
Acountry = makepolymodelA(c, q+1, 'normalize')
Atime    = makepolymodelA(t, p+1, 'normalize')

# Create Kronecker product of polynomial models
A = np.kron(Atime, Acountry)

# Make a 1D column vector from the 2D life expectancy data
y = (L.T).flatten().T

# Solve for coefficients of polynomial model
x = np.linalg.lstsq(A, y, rcond=None)[0]

# Compute fitted life expectancy values
yhat = A @ x
Lhat = yhat.reshape(t,c).T

# Plot original data and fitted surface
ax = plt.subplot(1,1,1, projection='3d')
Xcoords, Ycoords = np.mgrid[1:(c+1), 1800:(1800+t)]
ax.plot_surface(Xcoords, Ycoords, L, alpha=0.2, linewidth=0)
ax.plot_surface(Xcoords, Ycoords, Lhat, cmap='jet', linewidth=2)
ax.set_xlabel('Country #')
ax.set_ylabel('Year')
ax.set_zlabel('Life expectancy')
ax.view_init(57, 22)
ax.set_xlim(1, 183)
ax.set_ylim(1800, 2020)
ax.set_zlim(0, 90)
ax.set_xticks([1, 50, 100, 150, 183])
ax.set_yticks([1800, 1850, 1900, 1950, 2000, 2020])
ax.set_zticks([0, 10, 20, 30, 40, 50, 60, 70, 80, 90])
plt.show()
