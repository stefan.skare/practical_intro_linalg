import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

pd.set_option('display.max_rows', 200)

# Read CSV file and remove 1st column with country names
Ltable = pd.read_csv('c56_regressors_gapminderorg.csv')
Ltable = Ltable.iloc[:, 1:] # remove 1st column with country names

# Convert dataframe to 2D array
L = np.array(Ltable)

# Figure 1: Plot life expectancy and regressors as gray scale bars
plt.figure(1)
plt.gray()

for j in range(Ltable.shape[1]):
		plt.subplot(1, Ltable.shape[1], j+1)
		plt.imshow(L[:, j:j+1], aspect='auto')
		# plt.axis('off')
		if j == 0:
				plt.ylabel('Country index (sorted by life expectancy)')
		plt.xlabel(Ltable.columns[j])

plt.show()


# Figure 2: Scatter plot regressors vs life expectancy with fitted line
plt.figure(2)

y = L[:, 0]  # life expectancy is the 1st column
regressors = L[:, 1:]
# Get the names of the column headers of the table
regressor_names = list(Ltable.columns[1:])
const = np.ones(y.shape)

do_log_some_regressors = True  # True = yes, False = no. Why not try both?
if do_log_some_regressors:
	regressors[:, 2:] = np.log(regressors[:, 2:])
	regressor_names[2:] = ['log(' + x + ')' for x in regressor_names[2:]]

# Reference error norm 'Rconst'
# We can use this when calculating the relative error norm below
Rconst = np.linalg.norm(y - np.mean(y))

rcorr = np.zeros(regressors.shape[1])
R = np.zeros(regressors.shape[1])

for r in range(regressors.shape[1]):
  # model a constant + one regressor at a time
	A = np.column_stack((const, regressors[:, r]))
 
	xhat = np.linalg.lstsq(A, y, rcond=None)[0]
	yhat = A @ xhat
	tmp = np.corrcoef(regressors[:, r], y) # returns a 2x2 matrix, but ...
	rcorr[r] = tmp[0,1] # ... we need only one of the non-1 values in that matrix
	R[r] = np.linalg.norm(y - yhat) / Rconst # Relative error norm
	plt.subplot(2, 3, r+1)
	plt.plot(regressors[:, r], y, '.')
	plt.plot(regressors[:, r], yhat, '-')
	plt.xlabel(regressor_names[r])
	plt.ylabel('Life expectancy')
	plt.legend([f"R={R[r]:.3f}  r={rcorr[r]:.3f}"])
	plt.axis('tight')

plt.show()


# Figure 3: The relative error norm for one regressor at a time and combined
plt.figure(3)
for r in range(regressors.shape[1]):
	plt.plot([0, 1], [1, R[r]], 'o-')
	plt.text(1, R[r], regressor_names[r], verticalalignment='top')

plt.grid(True)
plt.xlabel('Number of regressors included in model matrix A')
plt.ylabel('Relative error norm R')

# Let's sort the regressors by their relative error norm from the fit to
# life expectancy. This moves the most correlating regressor to the 
# first column and the least to the last
# column. We will use this below to see what happens when more than one regressor is used
# in the model matrix A
# R = np.linalg.norm(y - const.reshape(-1,1), axis=0)
s = np.argsort(np.abs(R))
regressors_sorted = regressors[:,s]
regressor_names_sorted = [regressor_names[i] for i in s]

# Now, try adding more regressors and see if the relative norm decreases much further
Rcomb = np.zeros(regressors.shape[1])
for r in range(regressors.shape[1]):
  # increase the number of columns of A as 'r' increases
	A = np.hstack((const.reshape(-1,1), regressors_sorted[:,0:r+1]))
	xhat = np.linalg.lstsq(A, y, rcond=None)[0]
	yhat = A @ xhat
	Rcomb[r] = np.linalg.norm(y - yhat) / Rconst
	
	if r >= 1:
		plt.plot([r, r+1], [Rcomb[r-1], Rcomb[r]], 'ok-')
		plt.text(r+1, Rcomb[r]-0.005, 
           '\n'.join(regressor_names_sorted[0:r+1]), verticalalignment='top')

plt.axis([0, regressors.shape[1]+1, 0, 1])
plt.show()


# Figure 4: Make a correlation matrix between all vectors
plt.figure(4)
corrdata = np.hstack((regressors, y.reshape(-1,1)))
C = np.round(np.corrcoef(corrdata.T), 2)
plt.imshow(C, vmin=-1, vmax=1, cmap='jet')
plt.show()
