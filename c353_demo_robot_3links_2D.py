import numpy as np
import matplotlib.pyplot as plt
from c353_demo_robot_3links_2D_helper import *

# Initial angles of the joints of the robot
q0_start = 0;   q0 = q0_start
q1_start = 175; q1 = q1_start
q2_start = -45; q2 = q2_start

# How many degrees each joint should rotate from its starting position
q0_move = 23
q1_move = -79
q2_move = -48

stepsize = 0.02 # Step size for the plotting animation

# create a new figure and axis, which we send to 
# the plot_3links() to reuse the same window
plt.ion()
fig = plt.figure()
ax = fig.add_subplot(1,1,1)

# First animate the motor at p0 by rotating it q0_move degrees
# (from q0_start to q0_start+q0_move)
for t in np.arange(0,1+stepsize,stepsize):
  q0 = q0_start + q0_move * t # change q0 as t goes from 0 to 1
  p = get_3links_coords(q0, q1, q2)
  plot_3links(fig,ax,p)

# Then animate the motor at p1 by rotating it q1_move degrees
# (from q1_start to q1_start+q1_move)
for t in np.arange(0,1+stepsize,stepsize):
  q1 = q1_start + q1_move * t # change q0 as t goes from 0 to 1
  p = get_3links_coords(q0, q1, q2)
  plot_3links(fig,ax,p)

# Finally, animate the motor at p2 by rotating it q2_move degrees
# (from q2_start to q2_start+q2_move)
for t in np.arange(0,1+stepsize,stepsize):
  q2 = q2_start + q2_move * t # change q0 as t goes from 0 to 1
  p = get_3links_coords(q0, q1, q2)
  plot_3links(fig,ax,p)


# Alternatively, we can move all motors together
q0 = q0_start; q1 = q1_start; q2 = q2_start # reset to start position

for t in np.arange(0,1+stepsize,stepsize):
  q0 = q0_start + q0_move * t # change q0 as t goes from 0 to 1
  q1 = q1_start + q1_move * t # change q1 as t goes from 0 to 1
  q2 = q2_start + q2_move * t # change q2 as t goes from 0 to 1
  p = get_3links_coords(q0, q1, q2)
  plot_3links(fig,ax,p)