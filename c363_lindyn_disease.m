%% Chapter 3.6.3 – Disease modeling

%---- Probabilities per week (vaguely similar to the Covid19 pandemic)
Psi = 0.0008;
Pir = 0.35;
Pis = 0.05;
Pid = 0.004;
Prs = 0.03;

% Dynamics matrix
A = [(1-Psi) Pis              Prs     0
     Psi     (1-Pis-Pir-Pid)  0       0
     0       Pir              (1-Prs) 0
     0       Pid              0       1];
   
x0 = [8e9 0 0 0]'; % 8 billion susceptible people in the beginning
x(:,1) = x0;

numweeks = 52 * 2;  % simulate 2 years (104 weeks)

for t = 1:numweeks
  x(:,t+1) = A * x(:,t);
end

%-------------------------- Plotting the result --------------------------------

% line colors and indices as variable names (based on the A matrix)
cs = [231 34 123]/255; susceptible = 1; % susceptible
ci = [78 174 51]/255;  infected    = 2; % infected
cr = [185 75 23]/255;  recovered   = 3; % recovered
cd = [92 146 153]/255; dead        = 4; % dead

clf % clear figure

% left plot: susceptible and recovered in billions
subplot(121);hold on
plot(x(1,:)/1e9,'Color',cs,'LineWidth',4);
plot(x(3,:)/1e9,'Color',cr,'LineWidth',4);

set(gca, 'Linewidth', 1, 'FontName','Avenir', 'FontSize', 24, ...   
         'XLim',[0 inf], 'YLim',[0 10])
xlabel('Weeks after first infected case'); ylabel('Billion people (10^9)');
legend('x_s (susceptible)','x_r (recovered)','Location','NorthEast')

% right plot: susceptible and recovered in millions
subplot(122); hold on
plot(x(2,:)/1e6,'Color',ci,'LineWidth',4);
plot(x(4,:)/1e6,'Color',cd,'LineWidth',4);

set(gca, 'Linewidth', 1, 'FontName','Avenir', 'FontSize', 24, ...
         'XLim',[0 inf], 'YLim',[0 20])
xlabel('Weeks after first infected case'); ylabel('Million people (10^6)');
legend('x_i (infected)','x_d (dead)','Location','NorthEast')