%% Chapter 5.2.1 – Temperature modeling 20th century & condition number

% load temperature changes over the 20th century
tdata = readtable('c521_fitting_temp_data.csv');
years = 0:99; % One hundred years (1920-2019)

y = tdata.dt;

clf

option = 'normalize';

for p = 1:6 % polynomial degrees 0->5

  subplot(2,3,p);
  plot(years, y, 'ko-','linewidth', 1.5); hold on;

  % makepolymodelA(): Adding a third argument 'normalize' scales the column 
  % vectors of A to the same L2 norm
  % The matrix A becomes then more stable to invert (lower condition number)
  % To get the same values for xhat, we post-divide with these norm scalings
  [A, normscale] = makepolymodelA(years, p, option); % 'normalize' as 3rd arg

  % Condition number of A: Values above about millions means 
  % that small changes in A result in large (& unreliable) changes in xhat
  % In Matlab, cond(A) is the same as cond(inv(A'*A)*A') for tall matrices
  fprintf('p%d: condition number %.1f\n', p, cond(A))
  xhat = A \ y;
  yhat = A * xhat;

  if strcmp(option, 'normalize')
    fprintf('xhat corrected for the normadjusted columns of A\n')
    xhat = xhat ./ normscale;
  end
  fprintf('xhat: '); fprintf('%g ', xhat); fprintf('\n')

  rnorm = norm(y-yhat);

  plot(years, yhat, 'b-', 'linewidth', 5);
  xlabel('years after 1920')
  ylabel('Temperature change [C]')
  title(sprintf('Polynomial fit - degree %d', p-1));
  text(10, 0.8, {'Poly coeff. (x_{hat}) =' ; sprintf('%.5f\n', xhat)})
  text(50, 1, sprintf('||r||_2 = %.3f', rnorm));
  axis([-1 101 -0.2 1.5])
  axis square
  legend('y', 'y_{hat}', 'location', 'SouthEast')
  set(gca,'fontsize',20,'fontname','avenir')
end