%% Chapter 5.4.4 – Heathrow weather 72 years (linear, seasonal & hourly models)

% Load Heathrow weather since 1949
LHR = readtable('c544_heathrowweather.csv'); % table size: 487204x5

%% Convert date & time to decimal days and hours since 1949-01-01 @ 00:00:00
%  - We need to do this as the months have different number of days and
%    some years have leap days
days_in_solaryear = 365.256363; % https://en.wikipedia.org/wiki/Tropical_year
% number of hours per solar year:
hours_in_solaryear = round(days_in_solaryear * 24);

% For each observation, get number of (decimal) days & hours since the
% first measurement 
decimaldays = datenum(LHR.Date) + LHR.Hour/24;
decimaldays = decimaldays - decimaldays(1);
accumulatedhours = round(decimaldays * 24);
n = accumulatedhours(end)+1;
ndays = round(decimaldays(end));
nyears = round(ndays/days_in_solaryear);
decimalyears_incl_missingdata = linspace(0, nyears, n)';

% Fill in data for hours having observations since 1949. 
% size(y) = (hours between 1st and last) x (3 metrics)
Y = nan(n, 3); % Initialize with NaN to create 'keeprows' vector below 
               % (indicating hours with observations)
Y(accumulatedhours+1, 1) = LHR.Temperature;
Y(accumulatedhours+1, 2) = LHR.WindSpeed;
Y(accumulatedhours+1, 3) = LHR.AirPressure;

% true/false vector, where true means hours that have observation
% This is used to remove rows of the model matrix that have no measured data
keeprows = ~isnan(sum(Y,2)); 

%% 3 model matrices at different time scales 

%---- Linear modeling over 72 years
% since the 2nd column has
% - zero mean, it will not contribute to the mean
% - range 1, the value of xlin(2) will represent how much 
%   each metric changes over the entire period
Alin = [ones(n,1) linspace(-0.5, 0.5, n)'];

%---- Seasonal modeling (repeated over all 72 years)
Aoneyear = kron(eye(366), ones(24,1)); % first 366 days
% remove the last 18 hours of the 366th day
Aoneyear = Aoneyear(1:hours_in_solaryear,:); 
% stack Aoneyear vertically 'nyears' times
Ayear = repmat(Aoneyear, nyears, 1); 

%---- Hourly modeling (repeated over all 72 years)
Aday = repmat(eye(24), ndays, 1);


%% [Alin] - linear trend (over 72 years)
% mean + linear trend for temperature, wind speed and air pressure
% at the same time
Xlinhat = Alin(keeprows,:) \ Y(keeprows,:); % Xlinhat (2x3)
Yhat = Alin * Xlinhat;
figure(1);clf;
c544_fitting_heathrow_plotdata(decimalyears_incl_missingdata, Y, Yhat, Xlinhat)

%% [Ayears] - model seasonal effects after removing mean + linear trend
Y1 = Y - Yhat; % remove what was explained by the previous linear fit
Xyearhat = Ayear(keeprows,:) \ Y1(keeprows,:); % Xyearhat (366x3)
Y1hat = Ayear * Xyearhat;
Yhatboth = Yhat + Y1hat; % combining the linear fit and the seasonal variations
figure(2);clf;
c544_fitting_heathrow_plotdata(decimalyears_incl_missingdata, Y, Yhatboth, Xyearhat)

%% [Adays] - model hourly effects after removing seasonal effects and linear trend
Y2 = Y1 - Y1hat; % remove also the seasonal trends
Xdayhat = Aday(keeprows,:) \ Y2(keeprows,:);
Y2hat = Aday * Xdayhat;
Yhatall = Yhat + Y1hat + Y2hat; % combining fits: linear + seasonal + hourly variations
figure(3);clf;
c544_fitting_heathrow_plotdata(decimalyears_incl_missingdata, Y, Yhatall, Xdayhat)


%% Comparing norm ratios
ynorm = vecnorm(Y(keeprows,:));
% parts of data that is not constant:
y0 = vecnorm(Y(keeprows,:) - mean(Y(keeprows,:)));
% amount that cannot be explained by the linear model:
r  = vecnorm(Y(keeprows,:) - Yhat(keeprows,:));
% amount that cannot be explained by the linear+seasonal model:
r1 = vecnorm(Y(keeprows,:) - Yhatboth(keeprows,:));
% amount that cannot be explained by the linear+seasonal+hourly model:
r2 = vecnorm(Y(keeprows,:) - Yhatall(keeprows,:));

% How much we were able to explain of the variations in data 
% (mean values removed)
E = 100 * (1 - r2./y0);
fprintf('Explainability percentages\n');
fprintf('Temp.: %.1f Wind speed %.1f Air pressure %.1f\n', E(1), E(2), E(3));
