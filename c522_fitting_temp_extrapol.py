## Chapter 5.2.2 – Temperature modeling 1920-1990. Extrapolation 1990-2019
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from makepolymodelA import makepolymodelA

# load temperature changes over the 20th century
tdata = pd.read_csv('c521_fitting_temp_data.csv')
years = np.matrix(range(100)).T # One hundred years (1920-2019)

fityears = years[0:71] # 1920-1990
extrapolateyears = years[71:] # 1991-2019 "unseen"

# use only the first 71 elements (0->70) for fitting, 
# simulating what was known 1990
y100 = np.asmatrix(tdata.dt).T
y_fitthis = y100[0:71]
y_extrapolatethis = y100[71:]

option = 'normalize'

for p in range(1,7): # polynomial degrees 0->5
	
	ax = plt.subplot(2,3,p)
	plt.plot(fityears, y_fitthis, 'ko-', linewidth=2, label='y used in model')
	plt.plot(extrapolateyears, y_extrapolatethis, 'ko:', label='unseen y')
 
	# makepolymodelA(): Adding a third argument 'normalize' scales the column
	# vectors of A to the same L2 norm
	# The matrix A becomes then more stable to invert (lower condition number)
	# To get the same values for xhat, we post-divide with these norm scalings  
	A,normscale = makepolymodelA(fityears, p, option)
	xhat = np.linalg.lstsq(A, y_fitthis, rcond=None)[0]
	yhat_71years = A @ xhat
	if option == 'normalize':
		xhat = xhat / normscale
	rnorm = np.linalg.norm(y_fitthis-yhat_71years)
	
	A_100years,_ = makepolymodelA(years, p)
	yhat_100years = A_100years @ xhat
	rnorm100 = np.linalg.norm(y100 - yhat_100years)

	plt.plot(fityears, yhat_71years, 'b-', label='yhat,71 years', linewidth=5)
	plt.plot(years, yhat_100years, 'r--', linewidth=3, label='yhat,extrapol. to 100 years')

	ax.set_xlabel('years after 1920')
	ax.set_ylabel('Temperature change [C]')
	plt.text(20, 0.6, 'poly coeff:\n' + str(xhat.round(5)))
	plt.text(30, 0.6, '$||r||_2$ =' + str(round(rnorm100,3)))
	ax.set_xlim([-1, 101])
	ax.set_ylim([-0.2, 1.5])
	plt.legend(loc='upper left')

plt.show()