x = [1 3 7 10 12]'; % 5x1. (') makes it a column vector
y = [5 7 6 10 13]'; % 5x1. (') makes it a column vector

A = [x.^0 x.^1 x.^2 x.^3 x.^4];
c = inv(A) * y % => 5 polynomial coefficients

% make the polynomial interpolation curve (y_curve) using dense dots
xint = (min(x):0.1:max(x))'; % an array of min and max of x with step size 0.1
Aint = [xint.^0 xint.^1 xint.^2 xint.^3 xint.^4];
y_curve = Aint * c;

% plotting
hold off % overwrite next plot
plot(x, y,'ro') % the 5 points as red circles
hold on % add next plot
plot(xint, y_curve,'g-') % A green curve using step size 0.1
