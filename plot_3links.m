% Input: p, 2x4 array containing four 2D coordinates to plot
function plot_3links(p)
 
  % make a fixed black U-shape that resembles the target of interest
  hold off; plot([1 1.2 1.26 1.08], [2.05 2.1 2 1.87],'k-','linewidth', 2);
  hold on

  % plot the resulting coordinates or robotic link
  linkcolors = {[0.85 0.33 0.1], [0.47 0.67 0.19], [0 0.45 0.74]};
  for j = 1:3
    plot([p(1,j),p(1,j+1)], [p(2,j),p(2,j+1)],'-','Color', linkcolors{j})
  end

  % square axes, scale to +/- m, use grid
  m = 2.2;
  axis square; grid on; axis([-1 1 -1 1] * m); drawnow

end
