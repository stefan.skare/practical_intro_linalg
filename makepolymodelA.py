import numpy as np
import matplotlib.pyplot as plt

def makepolymodelA(n,p,option=""):
	if np.size(n) == 1:
		x = np.asmatrix(range(n)).T
	else:
		x = n
	A = np.matrix(np.zeros([np.size(x),p]))

	normscale = np.ones(p)[:,None] # [:,None] makes it a column-vector
 
	for k in range(p):
		col = np.power(x, k)
		if (option == "normalize"):
			# dividing by the norm makes the column vectors 
			# equally long and A becomes more numerically stable
			normscale[k] = np.linalg.norm(col)
			col = col / normscale[k]
		A[:,k] = col

	return (A, normscale)


if __name__ == "__main__":
		
	# For a dataset with 100 values, create a 5th degree polynomial model by
	[A,normscale] = makepolymodelA(100,6)
	print(normscale)
	plt.plot(A)
	plt.title('column vectors not normalized')
	plt.show()

	# To make the A matrix more numerically stable for inversion and easier to 
	# view in a plot, use 'normalize'
	[A,normscale] = makepolymodelA(100,6,'normalize')
	print(normscale)

	plt.title('column vectors normalized')
	plt.plot(A)
	plt.show()
