import numpy as np
import matplotlib.pyplot as plt

# Chapter 3.5.2 – Mass on a vertical spring with damping
# Based on: https://www.kalmanfilter.net/modeling.html

# Simulation parameters
m = 4                     # mass [kg]
d = 1                     # damper coefficient
s = 8                     # spring coefficient
g = -9.82                 # gravitational acceleration [m/s^2]
T = 10                    # max time [s]
h = 0.001                 # small update steps in [s]. Recommended <= 0.001
x0 = np.matrix([[0],[0]]) # Initial value: position = 0, speed = 0

# Dynamics matrix A
A = np.matrix([[0,1], [-s/m, -d/m]])

# Input matrix B
B = np.matrix([[0], [g]])

# Output matrix C (no effect here)
C = np.matrix([[0,0]])

# Feedthrough matrix D (no effect here)
D = 0

#### Update state vector using linear algebra in small steps ####

I = np.eye(2) # identity matrix
t = np.arange(0,T,h) # array with time steps

# size [2,np.size(t)] to plot the history over T
x = np.asmatrix(np.zeros((2,np.size(t))))

x[:,0] = x0 # assign x0 to the first column

# Update the state vector x[:,k+1] from x[:,k] for each time step k
for k in range(0,np.size(t)-1):
  x[:,k+1] = (A * h + I) @ x[:,k] + B * h # update equation 
                                          # (needs small 'h' to work well)

# Plot the result with dots (red for position, green for velocity)
skip = int(1/(10*h)) # plot every skip'th column in x to avoid the dots to overlap

fig,axs = plt.subplots(2)
axs[0].plot(t[::skip], x[0,::skip].T, 'ro')
axs[1].plot(t[::skip], x[1,::skip].T, 'go')
plt.show(block=False)

# OPTIONAL: Compare with the result with a continous-time solution using 'ss' 
# and 'lsim' (as accurate reference). https://python-control.readthedocs.io
# For this part of the script to work, 'control' and 'slycot' module needs 
# to be installed:
# conda install -c conda-forge control slycot

from control.matlab import ss,lsim

#### Accurate solution using functions 'ss' and 'lsim' (for reference) ####
tcont = np.arange(0,T,0.1)
u = np.ones(np.size(tcont))

sys = ss(A,B,C,D) # create state space model

# simulate time response of dynamic system
[dummy1,dummy2,xref] = lsim(sys,u,tcont,x0)

# plotting state solver result
axs[0].plot(tcont, xref[:,0], 'r-')
axs[1].plot(tcont, xref[:,1], 'g-')
plt.draw()
