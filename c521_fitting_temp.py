## Chapter 5.2.1 – Temperature modeling 20th century & condition number
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from makepolymodelA import makepolymodelA

pd.set_option('display.max_rows', 200)

# load temperature changes over the 20th century
tdata = pd.read_csv('c521_fitting_temp_data.csv')
years = np.matrix(range(100)).T # One hundred years (1920-2019)

y = np.asmatrix(tdata.dt).T
option = 'normalize'

for p in range(1,7): # polynomial degrees 0->5
	
	ax = plt.subplot(2,3,p)
	plt.plot(years, y, 'ko-',label='y')
	
	# makepolymodelA(): Adding a third argument 'normalize' scales the column
	# vectors of A to the same L2 norm
	# The matrix A becomes then more stable to invert (lower condition number)
	# To get the same values for xhat, we post-divide with these norm scalings
	A, normscale = makepolymodelA(np.size(y), p, option) # 'normalize' as 3rd arg here
	
	# Condition number of A: Values above about millions means 
	# that small changes in A result in large (& unreliable) changes in xhat
	# np.linalg.cond(A) is the same as np.linalg.cond(inv(A'*A)*A') 
	# for tall matrices
	print('p{0}: {1}\n'.format(p, round(np.linalg.cond(A),1)))
	
	xhat = np.linalg.lstsq(A, y, rcond=None)[0]
	yhat = A @ xhat
	
	if option == 'normalize':
		xhat = xhat / normscale
	
	rnorm = np.linalg.norm(y-yhat)
	plt.plot(years, yhat, 'b-',label='yhat', linewidth=4)
	ax.set_xlabel('year')
	ax.set_ylabel('Temperature change [C]')
	plt.text(20, 0.7, 'poly coeff:\n' + str(xhat.round(5)))
	plt.text(50, 1, '$||r||_2$ =' + str(round(rnorm,3)))
	ax.set_xlim([-1, 101])
	ax.set_ylim([-0.2, 1.5])
	plt.legend(loc='upper left')

plt.show()
