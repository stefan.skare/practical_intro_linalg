%% Chapter 5.3 – Bacteria modeling

% Scenario: Mostly number of bacteria for hours 0 to 50 in vector y
t = (0:50)';
y = [26 30 30 31 34 33 41 42 43 48 52 56 63 65 70 80 71 88 86 101 116 106 ...
    125 129 150 157 157 185 201 198 217 257 254 290 321 336 370 384 418 463 ...
    490 543 570 599 626 675 759 772 856 883 969]';

% Out of these, we have only counted the number of bacteria 
% for 6 consecutive hours ('m' for measured)
tm = t(21:26); % hours: 20 to 25
ym = y(21:26); % bacteria counts: 116 106 125 129 150 157

% Make a linear model Am for the measured interval
% Note that it is important here to use tm = 20:25 to get 'xhat' 
% scaled by absolute time. This allows us to make another matrix A 
% with t = 0:50 in its 2nd column to form yhat over the entire period
Am = makepolymodelA(tm, 2); % size 6x2

% linear model fit of the 2-log of the measured bacteria count
xhat = Am \ log2(ym);

% size 51x2. A linear model over the entire period, which we can...
A = makepolymodelA(t, 2);

% ...multiply with 'xhat'. 2 to the power of that becomes 'yhat' 
yhat = 2.^(A * xhat); % (which should match the true bacteria count)

clf
plot(t, y, 'ko', 'markersize',20,'linewidth',1); hold on;
plot(tm, ym, 'ro', 'markersize',20,'linewidth',3);
plot(t, yhat, 'b-', 'linewidth',3);

xlabel('time [h]')
ylabel('Bacteria count - B(t)')
title('Bacteria growth')
axis tight
grid on
legend('true bacteria count','measured bacteria count', 'yhat')
