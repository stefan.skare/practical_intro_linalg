function [A, normscale] = makepolymodelA(n,p,option)

if numel(n) == 1
  x = (0:(n-1))'; % make array of numbers 0 to (n-1)
else
  x = n; % use input array as x
end

for k = 1:p
  col = x.^(k-1);
  if exist('option','var') && strcmpi(option,'normalize')
    normscale(k,1) = norm(col);
    col = col / normscale(k);
  else
    normscale(k,1) = 1;
  end
  A(:,k) = col;
end

end
