import numpy as np
import matplotlib.pyplot as plt

# get_3links_coords()
# Input: Angles in degrees of the three rotating joints
# (located at origin, p1, and p2)
# Output: 2x4 vector containing four 2D coordinates to use 
# when plotting the three links
def get_3links_coords(q0, q1, q2):

  # Robotic links: [origin]---L0----[p1]---L1---[p2]-L2-[p3]

  # Lengths of each link of the robot
  L0 = 1.6 # length of first link (one end fixed to the origin)
  L1 = 1.4 # length of the second link (attached to the first and third links)
  L2 = 0.3 # length of the third link (attached to the second link).
           # The last link is also called the end-effector

  ### Transformation matrices needed (2 translation matrices, 3 rotation matrices)

  # T0 shifts the reference frame by the length of the first link
  T0 = np.array([[1,0,L0],[0,1,0],[0,0,1]]) 
  # T1 shifts the reference frame by the length of the second link
  T1 = np.array([[1,0,L1],[0,1,0],[0,0,1]])

  # np.radians to convert input in degrees to radians
  c0 = np.cos(np.radians(q0))
  s0 = np.sin(np.radians(q0))

  # rotation around the first joint
  R0 = np.array([[c0,-s0,0],[s0,c0,0],[0,0,1]])

  c1 = np.cos(np.radians(q1))
  s1 = np.sin(np.radians(q1))

  # rotation around the second joint (in the reference frame of the second joint)
  R1 = np.array([[c1,-s1,0],[s1,c1,0],[0,0,1]])
  c2 = np.cos(np.radians(q2))
  s2 = np.sin(np.radians(q2))

  # rotation around the third joint (in the reference frame of the third joint)
  R2 = np.array([[c2,-s2,0],[s2,c2,0],[0,0,1]])

  ### Calculate the four points

  # First point ('p0') is the origin
  p0 = np.array([[0,0,1]]).T # last 1 is the 1-extension, first two values = the origin coordinate

  # Second point ('p1') is where the first and second link meet
  p1 = np.array([[L0,0,1]]).T # Coordinate [L0,0]: The first link, horizontal with (0,0) being joint p0 (origin)
  p1 = R0 @ p1                # Rotate p1 around the origin using R0
  
  # Third point ('p2') is where the second and third link meet
  # Coordinate [L1,0]: The second link, horizontal in its own 
  # reference frame with (0,0) being joint p1 
  p2 = np.array([[L1,0,1]]).T 
  # Then multiply from right to left:
  # by R1 to rotate point p2 in its own reference frame by q1 degrees
  # by T0 to account for the length of the first link
  # by R0 to account for the rotation of the first joint at the origin
  # p2 is now defined in the same global reference frame as p0 and p1
  p2 = R0 @ T0 @ R1 @ p2      

  # Fourth point ('p3') is the tip of the third link, and the tip of the entire robot
  # Coordinate [L2,0]: The third link, horizontal in its own reference frame with (0,0) being joint p2 
  p3 = np.array([[L2,0,1]]).T      
  # Then multiply from right to left:
  # # by R2 to rotate point p3 in its own reference frame by q2 degrees
  # by T1 to account for the length of the second link
  # by R1 to account for the rotation of the second joint at p1
  # by T0 to account for the length of the first link
  # by R0 to account for the rotation of the first joint at the origin
  # p3 is now defined in the same global reference frame as p0, p1, and p2
  p3 = R0 @ T0 @ R1 @ T1 @ R2 @ p3 

  # return the all points in a 2x4 array
  p = [p0,p1,p2,p3]
  
  return p


# plot_3links()
# Input: fig handle, axes handle, 2x4 array with four 2D coordinates to plot
def plot_3links(fig, ax, p):

  ax.clear()

  # make a fixed black U-shape that resembles the target of interest
  ax.plot([1,1.2,1.26,1.08], [2.05,2.1,2,1.87],'k-')

  linkcolors = np.array([[0.85,0.33,0.1], [0.47,0.67,0.19], [0,0.45,0.74]]);

  # plot the resulting coordinates of robotic links
  for j in np.arange(0,3):
    plt.plot([p[j][0],p[j+1][0]], [p[j][1],p[j+1][1]], '-', color=linkcolors[j])

  m = 2.2
  ax.set_xlim([-m,m])
  ax.set_ylim([-m,m])
  ax.set_box_aspect(1) # square plot
  plt.grid(color='k', linestyle='-', linewidth=0.1)
  fig.canvas.draw()
  fig.canvas.flush_events()
