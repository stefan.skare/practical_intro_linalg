## Chapter 5.3 – Bacteria modeling
import numpy as np
import matplotlib.pyplot as plt
from makepolymodelA import makepolymodelA

# Assume we have some (mostly unknown) number of bacteria for hours
# 0 to 50 in vector y
t = np.matrix(range(51)).T # 0 to 50
y = np.matrix([[26,30,30,31,34,33,41,42,43,48,52,56,63,65,70,80,71,88,86,101,116,106,125,129,150,157,157,185,201,198,217,257,254,290,321,336,370,384,418,463,490,543,570,599,626,675,759,772,856,883,969]]).T

# Out of these, we have only counted the number of bacteria for 6 
# consecutive hours ('m' for measured)
tm = t[20:26] # hours: 20 to 25
ym = y[20:26] # bacteria counts: 116 106 125 129 150 157

# Make a linear model Am for the measured interval
# Note that it is important here to use tm = 20:25 to get 'xhat' scaled 
# by absolute time. This allows us to make another matrix A with t = 0:50 
# in its 2nd column to form yhat over the entire period
Am,_ = makepolymodelA(tm, 2) # size 6x2

# linear model fit of the 2-log of the measured bacteria count
xhat = np.linalg.lstsq(Am, np.log2(ym), rcond=None)[0]

# size 51x2. A linear model over the entire period, which we can...
A,_ = makepolymodelA(t, 2)

# ...multiply with 'xhat'. 2 to the power of that becomes 'yhat'
# (which should match the true bacteria count)
yhat = np.power(2, A @ xhat)

plt.plot(t, y, 'ko',label='true bacteria count')
plt.plot(tm, ym, 'ro',label='measured bacteria count')
plt.plot(t, yhat, 'b-',label='yhat')

plt.xlabel('time [h]')
plt.ylabel('Bacteria count - B(t)')
plt.title('Bacteria growth')
plt.legend(loc='upper left')
plt.show()