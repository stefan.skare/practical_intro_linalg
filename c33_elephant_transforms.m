%% Load and show elephant
clear; clf

load elephant.mat

whos % shows variables in workspace

% Variable 'elephant' size = 2 x 110 (110 points, each [x,y] coordinate)

axisrange = [-1 1 -1 1] * 2;

% show the original elephant
clf; plot(elephant(1,:), elephant(2,:),'r*-'); grid on; axis square; axis(axisrange);

%% 2D Transformation matrices (size 2x2)

% Mirror matrix - along x
Mx = [-1 0
       0 1];

% Mirror matrix - along y
My = [1 0
      0 -1];

% Scaling matrix S
sy = 1.2; sx = 0.9; % 1: no scale
S = [sx 0
     0 sy];

% Shearing matrix P
px = 0.2; py = 0.2; % 0: no shear
P = [1 px
     py 1];
   
% Rotation matrix R
theta = 30; % degrees
R = [cosd(theta)  sind(theta)
     -sind(theta) cosd(theta)];


%% Transform the elephant using mirror, scale, shear, and rotations

% [2x110] = [2x2] [2x110]
elephant_transformed = (Mx * S * R) * elephant; % the order matters (right->left)

clf;hold on
plot(elephant(1,:), elephant(2,:),'r*-'); % original elephant
plot(elephant_transformed(1,:), elephant_transformed(2,:),'b*-'); % transformed elephant
grid on; axis square; axis(axisrange);



%% Rotate the elephant from 0->359 degrees in steps of 5 deg
for theta = 0:5:359
  
  R = [cosd(theta)  sind(theta)
      -sind(theta) cosd(theta)];
  
  elephant_transformed = R * elephant; 
  
  clf; hold on
  plot(elephant(1,:), elephant(2,:),'r*-'); % original elephant
  plot(elephant_transformed(1,:), elephant_transformed(2,:),'b*-'); % transformed elephant
  grid on; axis square; axis(axisrange); drawnow
  
end

%% 1-extension of the elephant data to allow for 3x3 transformation matrices that can also do shifts

elephant(3,:) = 1; % adding a 3rd row with just ones below the x-y coordinate rows


%% 2D Transformation matrices using 1-extension (size 3x3)

% 2x2 => 3x3:
% adding a '0 0 1' 3rd row and a '0 0 1' 3rd column for all transformation matrices to make space
% for the translation effect using 1-extension

% Mirror matrix - along x
Mx = [-1 0 0
       0 1 0
       0 0 1];

% Mirror matrix - along y
My = [1  0 0
      0 -1 0
      0  0 1];

% Scaling matrix S
sy = 2; sx = 2; % 1: no scale
S = [sx 0 0
     0 sy 0
     0  0 1];

% Shearing matrix P
px = 0.2; py = 0.2; % 0: no shear
P = [1  px 0
     py  1 0
     0   0 0];
  
% Rotation matrix R
theta = 30; % degrees
R = [cosd(theta)  sind(theta) 0
     -sind(theta) cosd(theta) 0
          0            0      1];
 
% New: x-y translations (= shifts) with the 3rd column
dx = -0.4; dy = 1;
T = [1 0 dx
     0 1 dy
     0 0  1];
   

%% Transform the elephant using mirror, scale and shear. Now with 1-extension

% [3x110] = [3x3] [3x110]
MyTransform = T * R;
elephant_transformed = MyTransform * elephant; % the order matters (right->left)

% elephant_transformed:
% row 1: x-coordinates
% row 2: y-coordinates
% row 3: just 1s to allow us to translate (shift) the data using the 3rd column of our 3x3 tranformation matrix

% we can go back, by inverting our transform
elephant_transformed_back = inv(MyTransform) * elephant_transformed;


clf; hold on
plot(elephant(1,:), elephant(2,:),'r*-'); % original elephant
plot(elephant_transformed(1,:), elephant_transformed(2,:),'b*-'); % transformed elephant
plot(elephant_transformed_back(1,:), elephant_transformed_back(2,:),'go-'); % inverse transform of transformed elephant
grid on; axis square; axis(axisrange);





