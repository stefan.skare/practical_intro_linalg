%% Chapter 3.6.4 – Population modeling

% load population data vectors as a table from comma separated file
data2019 = readtable('swepopulationdata2019.csv');

% The loaded 100-element columns in table 'data2019' are:
% Column ages: 0:99
% Column x0:   population 2019
% Column b:    birth rate 2019
% Column d:    death rate 2019
% Column u:    net immigration 2019

%% Part 1: Show the loaded data (year 2019)
clf; clc
subplot(2,2,1); plot(data2019.ages, data2019.x0,'m');
title('Population 2019'); xlabel('Age')
subplot(2,2,2); plot(data2019.ages, data2019.b, 'b'); 
title('Birth rate 2019'); xlabel('Age')
subplot(2,2,3); plot(data2019.ages, data2019.d, 'g');
title('Death rate 2019'); xlabel('Age')
subplot(2,2,4); plot(data2019.ages, data2019.u, 'r');
title('Net immigration 2019'); xlabel('Age')

fprintf('Total population 2019: %.1f million\n',   sum(data2019.x0)/1e6);
fprintf('Number of births 2019: %.1f thousands\n', data2019.b' * data2019.x0/1e3);
fprintf('Number of deaths 2019: %.1f thousands\n', data2019.d' * data2019.x0/1e3);
fprintf('Net immigration 2019:  %.1f thousands\n', sum(data2019.u)/1e3);


%% Part 2: Population dynamics 2019-2100 (82 years total)
b = data2019.b; % use same birth rate as in 2019
d = data2019.d; % use same death rate as in 2019
u = 0.5 * data2019.u; % first try half (0.5*) the net immigration as in 2019
                 % try also other factors to see the effect on the population

simyears = 82; % 82 years (2019-2100)

% Build up the Leslie matrix ("A") from the birth and death rates of 2019
A = circshift(diag(1-d),[1 0]); % first create the values from the death rates
A(1,:) = b';                    % then add the birth rates as the first row of A

clear x
x(:,1) = data2019.x0; % set the first state of x as the 2019 population distribution

% Do the simulation
for k = 2:simyears
  x(:,k) = A * x(:,k-1) + u; 
end

clf; clc
fprintf('Total population 2019: %.1f million\n', sum(data2019.x0)/1e6);
fprintf('Total population 2029: %.1f million\n', sum(x(:,11))/1e6);
fprintf('Total population 2050: %.1f million\n', sum(x(:,32))/1e6);
fprintf('Total population 2100: %.1f million\n', sum(x(:,82))/1e6);

plot(data2019.ages, x(:,1), '-*')
hold on;
plot(data2019.ages, x(:,11)) % 11 = year 2029
plot(data2019.ages, x(:,32)) % 32 = year 2050
plot(data2019.ages, x(:,82)) % 82 = year 2100
legend('2019','2029','2050','2100')
xlabel('Age')
ylabel('# of people having this age')
